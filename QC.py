###############################################################################
"""
Project:      Aquatic Passage Barrier (APB) decision support tool 
Filename:     QC.py
Author:       Will Conley
Created on:   2022-01-02 
Usage:        module with quality control functions
Description:  n/a
Comments:     n/a
Dependencies: see requirements.txt
"""
###############################################################################

# package imports
import os
import csv
import chardet
import pandas as pd
import geopandas as gpd
import operator as op
import networkx as nx

# local imports
import AqBarrier_config as cfg


### naming conventions ###

grp_key = cfg.grp_key
src_key = cfg.src_key
bar_key = cfg.bar_key
bar_key_N = cfg.bar_key_N

# sets direction of segments
col_fr = cfg.col_fr
col_to = cfg.col_to


# instantiate geodataframe of stream geometry
src = gpd.read_file(cfg.src_strms)



### logging function ###

def log_result(k_lst, v_lst, col, result):
    """
    logs a result to a specified location
    """
        
    k_lst.append(col)
    v_lst.append(result)
    
    return k_lst, v_lst


### import/input functions ###

def encode_detect(in_csv):
    # look at the first ten thousand bytes to guess the character encoding
    
    with open(in_csv, 'rb') as rawdata:
        return chardet.detect(rawdata.read(10000))


def csv_to_UTF8(in_csv, out_csv='auto'):
    '''
    converts Excel-based csv to UTF-8
    '''
    if out_csv == 'auto':
        in_name_split = os.path.split(in_csv)
        index = in_name_split[1].find('.')
        out_name = in_name_split[0] + '\\' + in_name_split[1][:index] + '_UTF8' + in_name_split[1][index:]
    else:
        out_name = out_csv
                
    with open(in_csv, 'r', encoding='utf-8', errors='ignore') as infile, open(out_name, 'w', newline='', encoding='utf-8') as outfile:
        inputs = csv.reader(infile)
        output = csv.writer(outfile, delimiter=',')
        
        for row in inputs:
            output.writerow(row)
    
    return out_name


def null_dict(d):
    """
    tests if input is an empty dictionary
    needed b/c non-barrier nodes have null dicts
    """
    return not d    
    

### series functions ###

def unique_vals(s):
    """
    high-level (first-cut) test that a specified pandas series has no values occurring more than once
    """
    row_count = s.shape[0]
    unique_count = len(s.unique())
    return row_count == unique_count


### dataframe functions ###

def concat_from_to_count(df, col_fr=col_fr, col_to=col_to):
    """
    concatenates From and To columns, then counts number of times each from_to pair occurs 
    """
    
    df['FromTo'] = (df[col_fr].astype(str) + "_" + df[col_to].astype(str))

#     return pd.DataFrame(df['FromTo'].value_counts())
    return df.FromTo.is_unique


def chk_splitlength(df, leng_prec, src=src, src_key=src_key, grp_key=grp_key):
    """
    checks that sum of split seg lengths are within specified tolerance (leng_prec) of source parent segments
    """
    # create df with original source lengths
    dfsrc_leng_chk = src[[src_key, "Shape_Leng"]]
    dfsrc_leng_chk = dfsrc_leng_chk.set_index(src_key)
    dfsrc_leng_chk.sort_index(ascending=True, inplace=True)

    
    # create df that sums split legnths
    df_leng_chk = pd.DataFrame(df.groupby(grp_key).aggregate('LENGTH_GEO').sum())
    
    # merge dfs and confirm maximum difference is less than tolerance
    df_leng_mrg = dfsrc_leng_chk.merge(df_leng_chk, how="outer", left_index=True, right_index=True)
    df_leng_mrg['diff'] = df_leng_mrg.Shape_Leng - df_leng_mrg.LENGTH_GEO

    diff_max = df_leng_mrg['diff'].max()
    
    if diff_max <= leng_prec:
        print("The maximum difference between the sum of segments split by barriers and original line geometry is within specified precision. Proceed.")
    
    else:
        print("One or more summed segments split by barriers differs from original line geometry EXCEEDS specified precision. PROCEED CAUTIOUsLY.")

    return diff_max
    
    
def nodes_unique(df):
    """
    checks upstream and downstream nodes for uniqueness
    """
        
    k_lst, v_lst = [], []

    for c in df.loc[:,['node_us','node_ds']]:
        res = unique_vals(df[c])
        log_result(k_lst, v_lst, c, res)

    d = dict(list(zip(k_lst, v_lst)))
    
    if True in d.values():
        print('Downstream and/or upstream nodes are unique. Proceed.')
    else:
        print("Downstream and/or upstream nodes are NOT unique. DO NOT PROCEED.")
    
    return d


def edges_unique(df):
    """
    checks edges for uniqueness
    """
    if concat_from_to_count(df) is True:
        print('All edge combinations are unique. Proceed.')
    else:
        print('NOT all edge combinations are unique. DO NOT PROCEED.')


### graph functions ###

def find_selfloops(g):
    """
    Finds self-loops and returns list of related nodes
    """
    # create null list
    selfloop_nodelst = []

    # Iterate over all the edges of G
    for u, v in g.edges():

    # Check if node u and node v are the same
        if u == v:

            # Append node u to nodes_in_selfloops
            selfloop_nodelst.append(u)
    
    if not selfloop_nodelst:
        print("Graph has no self-loops. Proceed.")
    else:
        print("Graph has {} self-loops. Proceed at your own risk.".format(len(selfloop_nodelst)))
        
    return selfloop_nodelst


def bars_in_graph(df, g, warn=False):
    """
    determine which barriers in table participate as a graph node
    """
    nd_lst, g_part =[], []
    
    for nd in df[bar_key_N]:
        if nd not in list(g.nodes()):
            nd_lst.append(nd)
            g_part.append('No')
        else:
            nd_lst.append(nd)
            g_part.append('Yes')
    
    d = dict(list(zip(nd_lst, g_part)))
    
    if warn is True:
        if "No" not in d.values():
            print("All barriers from csv present in graph as nodes. Proceed.")
        else:
            print("There are {} barriers from csv not present in graph. Proceed cautiously.\nCheck for spatial duplicates and/or snapping tolerance in 1_preprocessing.".format(op.countOf(d.values(), 'No')))
    
    return d


def nodes_with_grtr_nbrs(nd_lst, g, m=2):
    """
    Returns all nodes in graph g that have something other than m neighbors.
    """
    nds = set()
    
    # Iterate over all nodes in g
    for n in nd_lst:
        # get list of neighbors
        nbrs = list(g.neighbors(n))

        # Check if the number of neighbors of n exceeds m         * THIS FUNCTION COULD BE MORE VERSATILE WITH A KWARG FOR THE OPERATOR *
        if len(nbrs) > m:

            # Add the node n to the set
            nds.add(n)
    
    if not nds:
        print("All barriers have two or fewer adjacent nodes. Proceed.")
    else:
        print("There is/are {} barrier(s) with {} or more adjacent nodes. Proceed at your own risk.".format(len(nds), m+1))
    
    # Return the nodes with m neighbors
    return nds


def is_bar_tst(n, g):
    """
    tests if node is a barrier, returns node ID and is_bar value (or 'N' if no value)
    n = node ID
    g = graph object
    """
    if null_dict(g.nodes[n]) == False and 'is_bar' in g.nodes(data=True)[n].keys():
        b = g.nodes[n]['is_bar']
    else:
        b = 'N'
    
    return n, b


# def get_us_nodes_excl(nd, g):
#     """
#     returns list of nodes upstream of a node (exclusive)
#     nd = node ID
#     g = graph object
#     """
#     return [n for n in nx.traversal.bfs_tree(g, nd, reverse=False) if n != nd]


# def get_us_nodes_incl(nd, g):
#     """
#     returns list of nodes upstream of a node (exclusive)
#     nd = node ID
#     g = graph object
#     """
#     return [n for n in nx.traversal.bfs_tree(g, nd, reverse=False)]


# def get_ds_nodes(nd, g):
#     """
#     returns list of nodes downstream of a node (inclusive)
#     nd = node ID
#     g = graph object
#     """
#     return [n for n in nx.traversal.bfs_tree(g, nd, reverse=True)]


# def edges_from_lst(nd_lst, g):
#     """
#     returns list of edges associated with members of a list of nodes
#     nd_lst = list of node IDs
#     g = graph object
#     """
#     return [(f,t) for f,t in g.edges if f in nd_lst or t in nd_lst]
