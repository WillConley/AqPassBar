###############################################################################
"""
Project:      Aquatic Passage Barrier (APB) decision support tool 
Filename:     Analysis.py
Author:       Will Conley
Created on:   2022-01-02 
Usage:        module with analytical functions
Description:  n/a
Comments:     n/a
Dependencies: see requirements.txt
"""
###############################################################################

import networkx as nx

# local imports
import QC
import AqBarrier_config as cfg


### naming conventions ###
bar_key_N = cfg.bar_key_N


### functions for individual nodes

def get_us_nodes_excl(nd, g):
    """
    returns list of nodes upstream of a node (exclusive)
    nd = node ID
    g = graph object
    """
    return [n for n in nx.traversal.bfs_tree(g, nd, reverse=False) if n != nd]


def get_us_nodes_incl(nd, g):
    """
    returns list of nodes upstream of a node (inclusive)
    nd = node ID
    g = graph object
    """
    return [n for n in nx.traversal.bfs_tree(g, nd, reverse=False)]


def get_ds_nodes_incl(nd, g):
    """
    * THIS CAN PROBABLY BE DEPRECATED * (see shortest_path_graph)
    returns list of nodes downstream of a node (inclusive)
    nd = node ID
    g = graph object
    """
    return [n for n in nx.traversal.bfs_tree(g, nd, reverse=True)]


# def tot_leng_us_bar(g):
#     """
#     * DEPRECATED * (replaced by get_geolength)
#     sums length of all successive edges to a given node
#     g = graph object
#     """
        
#     return sum(list(nx.get_edge_attributes(g,'LENGTH_GEO').values()))
    
    
### functions receiving multiple nodes

def edges_from_lst(nd_lst, g):
    """
    returns list of edges associated with members of a list of nodes
    nd_lst = list of node IDs
    g = graph object
    """
    return [(f,t) for f,t in g.edges if f in nd_lst or t in nd_lst]


def find_outlet_node(g):
    """
    returns the ID of the node (outlet node) with the most upstream nodes
    g = graph object
    """
        
    nd_id, us_nd_lst = [], []
    max_nd = 0

    for nd in list(g.nodes()):
        us_nodes = len(get_us_nodes_incl(nd, g))
        if us_nodes > max_nd:
            max_nd = us_nodes
        nd_id.append(nd)
        us_nd_lst.append(us_nodes)
    
    d = dict(list(zip(nd_id, us_nd_lst)))
    nd_max_id = max(d, key=d.get)
    
    return nd_max_id, d


def transfer_node_atts(t, g):
    """
    transfers attributes of common nodes from one graph to another
    t = target graph
    g = source graph
    """
    
    bar_lst, att_lst = [], []

    for nd in list(t.nodes):
        if QC.null_dict(g.nodes[nd]) is False:
            att_lst.append(g.nodes[nd])
            bar_lst.append(nd)

    d = dict(list(zip(bar_lst, att_lst)))
    
    nx.set_node_attributes(t, d)
    
    return t


def transfer_edge_leng(t, g):
    """
    transfers attributes of common edges from one graph to another
    t = target graph
    g = source graph
    """
    
    edge_lst, att_lst = [], []

    for e in list(t.edges(data=True)):
        leng = [v for k,v in g[e[0]][e[1]].items()][0]
        t[e[0]][e[1]]['LENGTH_GEO'] = leng

    return t


def shortest_path_graph(g, y, nd_atts=True, ed_leng=True):
    """
    creates a path graph between the catchment outlet and a specified target node
    g = parent graph
    y = target node
    """

    # identify shortest path between catchment outlet and barrier
    sp = nx.shortest_path(g, source=find_outlet_node(g)[0], target=y)

    # Create a graph from 'sp'
    pG = nx.path_graph(sp)  # does not pass edges attributes

    # conditional transfer of node attributes
    if nd_atts is True:
        transfer_node_atts(pG, g)
    else:
        pass
    
    # conditional transfer of edge length
    if ed_leng is True:
        transfer_edge_leng(pG, g)
    else:
        pass
    
    return pG
    

def get_geolength(g):
    """
    sums length values for all edges in a given graph
    g = graph
    """

    return sum(list(nx.get_edge_attributes(g,'LENGTH_GEO').values()))


def get_df_att(df, nd_lst, att, key=bar_key_N):
    """
    returns a list of values for a specified attribute (att) for a list of values for a specified column (key)
    NOTE: this assumes that att is present as a column (either in addition to or in lieu of index)
    """
        
    return df.query('{} in {}'.format(key, nd_lst))[att].tolist()
        


def get_graph_att_for_lst(g, att='all'):
    """
    returns a list of values for all (default) or a specified attribute from a graph if attributes exist
    """
    
    # if no argument is specified, return a list of dictionaries of all attributes for all nodes that have attributes
    if att == 'all':
        return [d for nd, d in list(g.nodes(data=True)) if QC.null_dict(d) is False]
    
    # if a valid attribute name is specified, return list of values for specified attribute for all nodes that have attributes
    else:
        return [d[att] for nd, d in list(g.nodes(data=True)) if QC.null_dict(d) is False]


def get_access_length(g, ndID):
    """
    sums the length of all edges upstream of a barrier of interest (BOI) to the headwaters,
        excluding edges upstream of other barriers

    tested against confirmed values in QGIS for nodes:
    - 131628: 27110 m
    - 131350: 10973 m
    - 131624:  7559 m
    - 132530:  7664 m
    - 134794:  1794 m
    - 131358:   154 m
    """
    Gsub = g.subgraph(get_us_nodes_incl(ndID, g))

    Gsub_unfrz = nx.DiGraph(Gsub)

    # identify first edge upstream of BOI
    first_edge = list(Gsub_unfrz.edges(ndID))

    # get list of barriers upstream of BOI
    usbars_unfrz = [n for n,b in Gsub_unfrz.nodes(data=True) if QC.null_dict(b) is False]

    # remove BOI from list 
    usbars_unfrz.remove(ndID)

    # compile lists of nodes upstream of all barriers for BOI
    usnd_lst_unfrz = []

    for b in usbars_unfrz:
        usnd_lst_unfrz.append(get_us_nodes_excl(b, Gsub_unfrz))

    # get unique list of all nodes upstream of all barriers within BOI's subcatchment
    usnd_lst_unfrz_unique = list(set(x for l in usnd_lst_unfrz for x in l))

    # get list of all edges assoicated with nodes upstream of barriers in subcatchment
    usedges_lst_unfrz = edges_from_lst(usnd_lst_unfrz_unique, Gsub_unfrz)

    # if edge immediately upstream of BOI is in upstream list, then remove from list
    # this block is important where another barrier is immediately upstream of BOI
    if first_edge in usedges_lst_unfrz:
        usedges_lst_unfrz.remove(first_edge)

    # remove edges upstream of subcatchment barriers
    Gsub_unfrz.remove_edges_from(usedges_lst_unfrz)

    # compute total length of remaining edges (i.e. stream length between initial barrier and closest upstream barriers)
    return get_geolength(Gsub_unfrz)

    
# def path_graph_lengths(g, y):
#     """
#     * DEPRECATED *
#     creates a path graph between the catchment outlet and a specified target node
#     assigns length values to edges from common edges of a parent graph
#     g = parent graph
#     y = target node
#     """
        
#     pG = shortest_path_graph(g, y)

#     # get length attributes for edges from parent graph
#     for e in list(pG.edges(data=True)):
#         leng = [v for k,v in g[e[0]][e[1]].items()][0]
#         pG[e[0]][e[1]]['LENGTH_GEO'] = leng

#     #### * TRANSFER NODE ATTRIBUTES HERE FOR GENERATING LIST OF DOWNSTREAM BARRIERS * ####
        
#     return tot_leng_us_bar(pG)

        
# def path_graph_with_lengths_from_lst(g, nd_lst):
#     """
#     * DEPRECATED *
#     creates a path graph between the catchment outlet and a target node from a list
#     assigns length values to edges from common edges of a parent graph
#     g = parent graph
#     nd_lst = list of target nodes
#     """
    
#     pth_leng = []
    
#     for y in nd_lst:
#         # identify shortest path between catchment outlet and barrier
#         sp = nx.shortest_path(g, source=find_outlet_node(g)[0], target=y)

#         # Create a graph from 'sp'
#         pG = nx.path_graph(sp)  # does not pass edges attributes

#         # get length attributes for edges from parent graph
#         for e in list(pG.edges(data=True)):
#             leng = [v for k,v in g[e[0]][e[1]].items()][0]
#             pG[e[0]][e[1]]['LENGTH_GEO'] = leng
        
#         pth_leng.append(tot_leng_us_bar(pG))
        
#     return pth_leng