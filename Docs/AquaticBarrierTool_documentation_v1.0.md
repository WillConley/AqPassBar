# Aquatic Passage Barrier (APB) Decision Tool

**The Aquatic Passage Barrier (APB)** decision tool is an open-source software tool for characterising and prioritising aquatic barrier (e.g. fish passage barriers) mitigation within catchments. 

It is created by Will Conley for Horizons Regional Council with funding from the New Zealand Public Waterways and Ecosystem Restoration Fund (project ID PWER009).

## Tool Intent

__APB__ is intended to address the questions of A) how much bang-for-your buck mitigating a particular barrier provides and B) in what order should barriers should be mitigated. The network analysis component is built upon [NetworkX](https://networkx.org/), a Python package for graph analysis. This provides speed and dependability for current scope while also affording scalability for future development (e.g. weighting of multi-species and/or life-history criteria).

## Repository

__APB__ is publicly available and hosted at https://gitlab.com/WillConley/AqPassBar

There is currently no budget for maintenance or development. Please get in touch if interested in contributing.

## Folder Structure

<ul>
|-- Home<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp|-- Data<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp |-- Outputs<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp|-- Docs<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp |-- Pics<br>
</ul>


## Tool Structure

__APB__ has four main elements:

<p>
<details>
<summary><font size=3><b>1) config file</b></font></summary>

__AqBarrier_config.py__ is the primary point-of-entry where users specify location and names of input files as well as set parameters such as snapping tolerance and horizontal precision. __AqBarrier_config.py__ is located in the __Home__ directory.
</details> 
</p>

<p>
<details>
<summary><font size=3><b>2) notebooks (*.ipynb)</b></font></summary>

- __1_preprocessing.ipynb__: This notebook is where geoprocessing occurs. It <u>__has an arcpy dependency__</u> (i.e. it a valid ArcGIS license and must be run in an arcpy-enabled Python 3 environment). __1_preprocessing.ipynb__ is located in the __Home__ folder. It is easiest to run from a ArcGISPro-managed python environment.

- __2_production.ipynb__: This notebook performs network characterisation and prioritisation. It is based on NetworkX and includes QC blocks to ensure data suitability. __2_production.ipynb__ is located in the __Home__ folder and <u>__does not have an arcpy dependency__</u>.
</details>
</p>

<p>
<details>
<summary><font size=3><b>3) modules (*.py)</b></font></summary>

- __QC.py__ contains functions primarily used for quality control and is located in the __Home__ folder.

- __Analysis.py__ contains functions for characterising, analysing, and prioritising barriers. It is located in the __Home__ folder.
</details>
</p>

<p>
<details>
<summary><font size=3><b>4) "Data" subfolder</b></font></summary>

The __Data__ subfolder this is where __APB__ looks for input data and (by default) writes output data.
</details>
</p>


## Input / Output

### 1_preprocessing.ipynb (optional)

If network topology is not currently organised as nodes (barriers and channel confluences) and edges (discrete, non-duplicate lines between barrier-to-barrier, barrier-to-confluence, confluence-to-barrier, or confluence-to-confluence) then you will need to run this notebook. For example, if you are starting with shapefiles, then you will need to run this notebook. It has an arcpy dependency so a valid ArcGIS license is required.

Running __1_preprocessing.ipynb__ generates a graph representation of the stream-barrier network by enforcing barriers onto stream lines (top-left of graphic). Once performed, barriers will occur as endpoints (nodes) and each line (= edge) will have a unique ID. If edges of the stream-barrier network already exist, then it is not necessary to run this notebook.

![image](../Docs/Pics/StreamNetworkExampleCompare_GIS_Graph_NZSegmentID.png)

Three input files are required:
- a point shapefile (.shp) of __barriers__ 
- a point shapefile (.shp) of __stream nodes__ (i.e. endpoints, such as confluences)
- a line shapefile (.shp) of simple __stream lines__. 

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; * *The notebook __will not run__ with multipart features.**

One file is output:
- a csv file of __edges__ with 'from' and 'to' node IDs suitable for use in NetworkX. By default, it has the following attribute fields:
&nbsp; <ul>
	<li><b>OID_</b> - ID value (unique, integer) of each edge  </li>
	<li><b>nzsegment</b> - ID value (non-unique, integer) from field of the same name of corresponding REC2 line segment</li>
	<li><b>LENGTH_GEO</b> - geometric length (in input units) of the corresponding barrier-enforced line segment</li>
	<li><b>XY_start</b> - coordinate value of starting node (string, "_"- delimited; e.g. 1802437.1211_5501319.7001)</li>
	<li><b>XY_end</b> - coordinate value of ending node (string, "_"- delimited; e.g. 1802932.2091_5501274.7008)</li>
	<li><b>nodID_strt</b> - ID value (unique, integer) of starting node</li>
	<li><b>nodID_end</b> - ID value (unique, integer) of ending node</li>
</ul>


### 2_production.ipynb (compulsory)

The purpose of __2_production.ipynb__ is to identify priority barriers. Combined with the modules/functions it calls, it is the core component of __APB__.

Two input files are required:
<ol>
<li>a csv file of __edges__ that reflects the stream-barrier network. That is,  the stream network it is partitioned by barriers. In other words, each record has a 'start' (or 'from') and an 'end' (or 'to') node ID, a unique identifier, and a geometric length. Each edge is defined by exactly two nodes. A node may be a stream junction (e.g. confluence) or a barrier, but should not be both. The following graphic depicts stream geometry in GIS with field-collected (GPS) barrier locations (left) into network topology (right). NOTE: While sequence of edge identifiers (edge_ID) is unimportant, start-end nodes must progress in a consistent direction (i.e. with the end node of one edge becoming the start node for the next edge, except where terminal).

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![image](../Docs/Pics/NodeEdgeTopology_120dpi.png)</li>

<li>a csv file of __barriers__. Each barrier must have a the following two atrributes at a minimum:
&nbsp; <ul>
	<li>unique identifier, and</li>
	<li>an estimate of mitigation cost</li>
</ul>
</ol>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![image](../Docs/Pics/Barriers_ExampleMinimumAttributeTable.png)

For the "production" file to return proper outputs, barriers must be imposed onto the stream network beforehand (e.g. via 1_preprocessing.ipynb) such that:
<ol>
<li>each network node is either a stream confluence (a.k.a. junction) or a barrier, AND</li>
<li>each real-world stream segment is represented by only one edge (i.e. no overlapping or duplication).</li>
</ol>

Output attributes are appended to the input barrier csv file.


## Usage

<b> * The config file is intended to be the sole/primary place for user-modification. * </b>

The tool has two components which may be run in-sequence or independently. 

<ul>
<li>If starting with GIS-style data (e.g. where real-world coordinates and geometry define the relations between barrier points and stream lines represented as separate 'layers'), you will need to transform data to node-edge relationships suitable for network representation via <b>1_preprocessing.ipynb</b>.</li>

<li>if you already have tabular data in node-edge form, you <i>may</i> be able to proceed straight to <b>2_production.ipynb</b>. There are several code blocks that will check the data for suitability (e.g. unique edges) and advise if one should not proceed.
<ol>
<li>each network node is either a stream confluence (a.k.a. junction) or a barrier, AND</li>
<li>each real-world stream segment is represented by only one edge (i.e. no overlapping or duplication).</li>
</ol>
</ul>

### Preparing the Python Environment

There are many ways to prepare a Python environment. This section is intended for novice python users in Windows.

The <b>recommended approach</b> is to create an environment from the provided <b>requirements.txt</b> file in the <b>Home</b> folder. Open a Python Command Prompt. If planning to run <b>1_preprocessing.ipynb</b>, do this from within the ArcGIS folder in the Windows Start Menu:

![image](../Docs/Pics/WindowsStartMenu_ArcGIS_PythonCommandPrompt.png)

<p>
Copy the <b>Home</b> folder and all contents into an arcpy-enabled Windows <b>Users</b> folder (this can be done with Windows Explorer). For example, here I have copied into a new folder called "APB_tool" that is now my <b>Home</b> folder:

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;C:\Users\Will\AppData\Local\ESRI\conda\envs\APB_tool
</p>

From the Python Command Prompt, change the working directory to the arcgis-enabled directory within the appropriate Windows <b>Users</b> folder.

Then, create a new environment using the <b>requirements.txt</b> file in the <b>Home</b> folder. The generic implementation looks like:

    conda create --name <your env name> -- file <requirements file>

In my case, I create an environment named "APB_env" from the requirements file in the new "APB_tool" folder I created above.

    conda create --name <your env name> -- file APB_tool\requirements.txt

![image](../Docs/Pics/ArcGIS_PythonCommandPrompt_List_Create_redact.png)



Alternatively, a GUI-based approach may be used, though it will be less reliable for ensuring package dependencies match the development environment. Start an ArcGIS Pro instance.

![image](../Docs/Pics/ArcPro_PyEnv.png)

<p>
Installed packages can be viewed and missing packages can be installed manually. If installing packages, it is recommended to clone the default environment, activate the cloned environment, then install packages into the cloned environment. Key dependencies include:
<ul>
<li> fiona v1.8.20
<li> geopandas v0.9.0
<li> networkx v2.5
<li> pandas v1.2.3
<li> shapely v1.7.1
</p>

![image](../Docs/Pics/ArcPro_PyPackages.png)


## Worked Example

If starting with shapefiles, then <b>1_preprocessing.ipynb</b> will need to be run. Assuming shapefile attribute tables meet criteria specified above, then:
<ol>
<li><b>Start a jupyter notebook instance:</b>
<ul> 
<li>Start an arcpy-enabled Python Command Prompt (see above section for <b>Preparing the Python Environment</b>).
<li>Change the working directory to the <b>Users</b> folder where you created the environment.

    (arcgispro-py3) C:\Program Files\ArcGIS\Pro\bin\Python\envs\arcgispro-py3>cd C:\Users\<username>\AppData\Local\ESRI\conda\envs

<li>Activate the new environment:

    (arcgispro-py3) C:\Users\<username>\AppData\Local\ESRI\conda\envs>activate APB_env

<li>Start a Jupyter Lab instance.

    (APB_env) C:\Users\<username>\AppData\Local\ESRI\conda\envs>jupyter lab

</ul>

<li> <b>Edit the config file:</b>
<ul> 
<li>From Jupyter Lab, open the folder you copied the APB files to, in my case, I called it <b>APB_tool</b>.

![image](../Docs/Pics/jupyterlab_home.png)
 
<li>Open and edit <b>AqBarrier_config.py</b>

![image](../Docs/Pics/jupyterlab_AqBarrier_config.png)

Variables and parameters are well documented with comments, but key ones to be aware of for user editing include:

<ul>
<li> work_dir - Is the folder containing input data.
<li> out_subdir - By default, is assumed to be a subdirectory of the work work_dir. If it doesn't exist, it will be created.
<li> in_pts_bars - Name of the barriers (points) shapefile.
<li> bar_ID - Is the  unique ID field within in_pts_bars
<li> in_pts_strms - Is the filename of streamnode shapefile (points; assumed to already be snapped to the ends of in_lines).
<li> strm_pt_ID - Is the unique ID field within in_pts_strms.
<li> in_lines - Is the filename of stream segments shapefile (lines; must be single-part)
<li> strm_seg_ID - Is the unique ID field within in_lines.
<li> snap_tol - snapping tolerance (if using the preprocessing notebook)
<li> src_bars_supp - the csv file with barrier cost values (and, potentially, other inforamtion). It must have a field matching bar_ID. Any barriers in the shapefile lacking values in src_bars_supp are excluded.
<li> hubID - is the node ID of the point to which connectivity is enforced if using the Downstream Optimisation approach. 
<li> cost_cap - The maximum amount of money to be spent.
</ul> 

</ul>

<li><b>Run the notebook(s)</b>

Within Jupyter Lab, run one or both notebooks. If running both:

<ol>
<li>open (e.g. double-click on the left panel) <b>1_preprocessing_NodesEdges_from_StreamsBarriers.ipynb</b>
<li>Run through the notebook by executing each code block using the Shift-Enter keyboard shortcut.
This will generate a csv for the following notebook.
<li>open (e.g. double-click on the left panel) <b>2_production.ipynb</b>
<li>Run through the notebook by executing each code block using the Shift-Enter keyboard shortcut. 
</ol>



Processing can occur in two ways:
<ul>
<li> <b>Downstream optimisation</b>: Identifying the longest cumulative segments that create connectivity to specified point within a specified cost cap. To run, ensure that the code block following the <b>Downstream Optimisation</b> header block is <b>active <i>AND</i></b> the code block following the <b>Maximum Length</b> header block is <b>inactive</b>.
<li> <b>Maxiumum Length</b>: Identifying the longest stand-alone segments within a specified cost cap. To run, ensure that the code block following the <b>Maximum Length</b> header block is <b>active <i>AND</i></b> the code block following the <b>Downstream Optimisation</b> header block is <b>inactive</b>.
</ul>

<li><b>Results</b> 

The output is a csv file to the specified output directory. The file maintains fields (including unused ones) from the csv file with barrier cost values (src_bars_supp), including the bar_ID field which can then be used to integrate APB output into GIS by joining to the barrier shapefile via the bar_ID field.

<ul>
<li><b>Maximum length</b>

This approach finds the longest edge, then checks the mitigation cost estimate for the downstream barrier of that segment against the cost cap. If the mitigation estimate is within the cap, it retains the segment and proceeds to the next longest segment, accumulating length and cost until the cap is reached. If the longest segment exceeds the cap, it proceeds to the next longest, and so forth. NOTE: This approach can result in isolated segments that don't necessarily restore broader connectivity on a network scale. Joining the APB output csv back into GIS facilitates cartographic representation of results (the following map was created in QGIS).

![image](../Docs/Pics/Ohau_PriorityMap_MaxLength.png)

This produces a well-ordered statistical distribution in terms of length priorty. An existing code block within APB can generate the following graphic.

![image](../Docs/Pics/Ohau_PriorityPlot_MaxLength.png)

<li><b>Downstream Optimisation</b>

This approach considers downstream connectivity and enforces paths that connect continuously downstream. It  begins with the downstream barrier of the longest edge having a mitigation cost less than the cost cap, then accumulates all unmitigated downstream barriers. It continues to iterate through barriers in order of longest immediate upstream edge length until the accumulated list of barriers exceeds the cost cap. This ensures a more connected geopraphic distribution. Joining the APB output csv back into GIS facilitates cartographic representation of results (the following map was created in QGIS).

![image](../Docs/Pics/Ohau_PriorityMap_DownstreamOptimised.png)

However, the statistical distribution may be less well-ordered (in terms of length priority) because it is often necessary to fix barriers controlling very short stream segments (i.e. edges) if they are situated low in the catchment. An existing code block within APB can generate the following graphic.

![image](../Docs/Pics/Ohau_PriorityPlot_DownstreamOptimised.png)

</ul>
</ol>

