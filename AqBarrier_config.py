###############################################################################
"""
# Project:      Aquatic Passage Barrier (APB) decision support tool 
# Filename:     AqBarrier_config.py
# Purpose:      input variables and parameters for stream-barrier network analysis    
# Author:       Will Conley
# Created on:   2021-11-10
# last edited:  2022-06-27
# Usage:        primary user interface for specifyin user inputs
# Description:  n/a
# Comments:     n/a
# Dependencies: see requirements.txt
"""
###############################################################################

########################     edit values as-needed     ########################
########################   do NOT edit variable names  ########################

###############################################################################


##### inputs for 1_preprocessing.ipynb #####

## set folders
work_dir = "C:\\Users\\Will\\AppData\\Local\\ESRI\\conda\\envs\\APB_tool\\Data\\" # folder with input data
# work_dir = "C:\\Users\\Will\\GitKraken\\hrc_fishpassage\\Data\\" # folder with input data
out_subdir = "Outputs\\" # folder with input data


## names of input files within work_dir and respective fields containing unique IDs
in_pts_bars = 'Ohau_barriers.shp' # barriers feature class (points)
bar_ID = "Barrier_NI" # unique ID field within in_pts_bars

in_pts_strms = 'Ohau_streamnodes_v5.shp' # streamnode feature class (points; assumed to already be snapped to the ends of in_lines)
strm_pt_ID = "OBJECTID" # unique ID field within in_pts_strms

in_lines = 'Ohau_streams.shp' # stream segments (lines, single-part)
strm_seg_ID = "nzsegment" # unique ID field within in_lines


## parameters
ref = 'EPSG:2193' # spatial reference
snap_tol = 200 # snapping tolerance
searchRadius = "{} Meters".format(snap_tol) # search distance and units from stream segments to barriers (formatted as string)
xy_tol = "0.01 Meters" # Set the XY tolerance within which to identical nodes will be deleted
z_tol = "" # Set the Z tolerance ("" = default)


## delete intermediate files?
del_files = True # choices are True or False


## output file (* IF EDITING the following, MUST CHANGE INPUT FILE for 2_production to match *)
l_pref, l_suff = str.split(in_lines,".")
lines_split = out_subdir + l_pref + "_split." + l_suff
pthpref, suff = str.split(lines_split, ".")
outpth, pref = str.split(pthpref, "\\")
out_csv1 = "{}_edges.csv".format(pref)


##### inputs for 2_production.ipynb #####

# csv of barriers from NIWA with supplemental attributes (e.g. cost)
src_bars_supp = work_dir + "20211102_NIWABarriers_Ohau_with_supplemental_fields.csv"

##
# out_csv = work_dir + out_subdir + "test_out_new.csv"
out_csv2 = "test_out_new.csv"

## other parameters
leng_prec = 1 # allowable units (same as data) of length disagreement (b/c lengths from csv are geodetic and differ than REC "Shape_Leng")
hubID = 1 # desired ID value of central/unifying feature () if multiple catchments considered
cost_cap = 100000 # maximum amount of money for remediation



############################# editing below this line will break 2_production ###################################

### naming conventions

# file names
cutlines = work_dir + out_subdir + "splitlines.shp"

# field names
grp_key = 'nzsegment' # non-unique key that links split segments back to REC attributes
src_key = 'nzsegment' # field with unique, common ID values to processed csv
bar_key = 'Barrier_NI' # unique IDs from NIWA attribute table that links barrier attributes to nodes
bar_key_N = 'Barrier_NIWA_ID' # renamed column of unique NIWA IDs for supplemental node table
old_leng = 'Length_old' # unsplit length
new_leng = 'LENGTH_GEO' # split length

# sets direction of segments
col_fr = 'node_ds' 
col_to = 'node_us'

# checking lengths and, if desired, joining attributes
src_strms = work_dir + in_lines

# csv of from-to pairs where the barriers have been enforced as nodes on stream lines
src_edges_enf = work_dir + out_subdir + out_csv1 
